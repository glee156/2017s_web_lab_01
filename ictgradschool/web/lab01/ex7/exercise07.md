### Benign herbivores

* Cat
* Dog
* Horse
* Cow
* Sheep
* Rat
* Capybara
* Chicken

### Evil carnivores
* Polar Bear
* Frog
* Rat
* Hawk
* Lion
* Fox

### In-between omnivores
* Raccoon
* Fennec Fox